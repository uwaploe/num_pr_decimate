
#include <vector>
#include <gtest/gtest.h>
#include <opencv2/core.hpp>

#include "test_data.h"

#include "num_pr_decimate/num_pr_decimate.h"


namespace {

  using namespace Numurus;
  using namespace PLDecimate_Test;

  template<typename T>
  void checkMatricesEqual( const cv::Mat &a, const cv::Mat &b ) {

    ASSERT_EQ( a.dims, 4 );
    ASSERT_EQ( a.dims, b.dims ) << "Matrices not same dimensionality " << a.dims << " != " << b.dims;

    for( unsigned int i = 0; i < a.dims; ++i ) {
      ASSERT_EQ( a.size[i], b.size[i] ) << "Size " << i << " not equal: " << a.size[i] << " != " << b.size[i];
     }

     const auto sz( a.size );
     cv::Vec<int,4> at(0,0,0,0);

     for(  at[0] = 0 ; at[0] < sz[0]; ++at[0] ) {
       for(  at[1] = 0; at[1] < sz[1]; ++at[1] ) {
         for(  at[2] = 0; at[2] < sz[2]; ++at[2] ) {
           for(  at[3] = 0; at[3] < sz[3]; ++at[3] ) {

             ASSERT_FLOAT_EQ( a.at<T>(at), b.at<T>(at) ) << "Mismatch at (" << at[0] << "," << at[1] << "," << at[2] << "," << at[3] << ") a = " << a.at<float>(at) << "; b = " << b.at<float>(at);
           }
         }
       }
     }



    const double n = cv::norm( a, b, cv::NORM_L2 );
    ASSERT_DOUBLE_EQ( 0.0, n );
  }

  TEST(TestConstructor, DefaultConstructor)
  {
    PLDecimate pr;
  }

  // Default params is all 1s, should leave matrix unchanged
  TEST(TestDefaultParams, TestFloatData)
  {
    cv::Mat input = testData<float>( TEST_DATA_FOURS );

    PLDecimate pr;
    cv::Mat output;
    float qualityIn = 1234.5, qualityOut;

    ASSERT_TRUE( pr.process( &input, &output, qualityIn, &qualityOut ) );

    ASSERT_FLOAT_EQ( 1.0, qualityOut );

    checkMatricesEqual<float>( output, input );
  }


  TEST( TestDecimate, TestEvenDecimation )
  {

    std::array<int,3> decimateValues = {1,2,4};

    for( const auto t : decimateValues ) {
      for( const auto r : decimateValues ) {
        for( const auto c : decimateValues ) {
          for( const auto chan : decimateValues ) {

            char keyChar[30];
            snprintf( keyChar, 30, "fours_decimate_%1d%1d%1d%1d", t, r, c, chan );
            std::string keyName( keyChar );

            cv::Mat input( testData<unsigned char>( TEST_DATA_FOURS ) );
            cv::Mat refResult = testData<unsigned char>( keyName );

            ASSERT_FALSE( refResult.empty() );

            PLDecimate pr;

            cv::Mat output;
            float qualityIn = 1234.5, qualityOut;

            PLRoutine::Config config;
            config.name_ = "PRDecimate";
            config.description_ = "PRDecimate";

            config.int_params[PLDecimate::PLDecimateConfig::TimeDecimateName] = t;
            config.int_params[PLDecimate::PLDecimateConfig::RowDecimateName] = r;
            config.int_params[PLDecimate::PLDecimateConfig::ColDecimateName] = c;
            config.int_params[PLDecimate::PLDecimateConfig::ChannelDecimateName] = chan;

            ASSERT_TRUE( pr.configure(config) );
            ASSERT_TRUE( pr.process( &input, &output, qualityIn, &qualityOut ) );

            // \TODO.   Actually calculate and check qualityOut
            ASSERT_NE( qualityIn, qualityOut );


            checkMatricesEqual<unsigned char>( output, refResult );

          }
        }
      }
    }
  }


  TEST( TestDecimate, TestUnevenDecimation )
  {

    std::array<int,3> decimateValues = {1,2,3};

    for( const auto t : decimateValues ) {
      for( const auto r : decimateValues ) {
        for( const auto c : decimateValues ) {
          for( const auto chan : decimateValues ) {


            // char keyChar[30];
            // snprintf( keyChar, 30, "fours_decimate_%1d%1d%1d%1d", t, r, c, chan );
            // std::string keyName( keyChar );

            cv::Mat input( testData<unsigned char>( TEST_DATA_FOURS ) );
            //cv::Mat refResult = testData<float>( keyName );
            //ASSERT_FALSE( refResult.empty() );

            PLDecimate pr;

            cv::Mat output;
            float qualityIn = 1234.5, qualityOut;

            PLRoutine::Config config;
            config.name_ = "PRDecimate";
            config.description_ = "PRDecimate";

            config.int_params[PLDecimate::PLDecimateConfig::TimeDecimateName] = t;
            config.int_params[PLDecimate::PLDecimateConfig::RowDecimateName] = r;
            config.int_params[PLDecimate::PLDecimateConfig::ColDecimateName] = c;
            config.int_params[PLDecimate::PLDecimateConfig::ChannelDecimateName] = chan;

            ASSERT_TRUE( pr.configure(config) );
            ASSERT_TRUE( pr.process( &input, &output, qualityIn, &qualityOut ) );

            /// \TODO.   Add actual predicted qualityOut to testing
            ASSERT_NE( qualityIn, qualityOut );

            //checkMatricesEqual( output, refResult );

            ASSERT_EQ( output.type(), input.type() );
            ASSERT_EQ( output.dims, input.dims );
            ASSERT_EQ( output.dims, 4 );

            // No value checking yet, just sizes
            auto const sz( output.size );
            std::array<int,4> decimation = {t,r,c,chan};

            for( unsigned int i = 0; i < 4; ++i ) {
              if( decimation[i] == 1 )
                ASSERT_EQ( 4, sz[i] ) << "For decimation " << decimation[0] << "," << decimation[1] << "," << decimation[2] << "," << decimation[3] << " on axis " << i << " output is size " << sz[i] << "!=4";
              else
                ASSERT_EQ( 2, sz[i] ) << "For decimation " << decimation[0] << "," << decimation[1] << "," << decimation[2] << "," << decimation[3] << " on axis " << i << " output is size " << sz[i] << "!=2";
            }

          }
        }
      }
    }


  }



}
