#pragma once

#ifndef TEST_DATA_PATH
  #error "TEST_DATA_PATH must be defined for unit test data to be found"
#endif

#include "test_data_impl.h"


/// Test data sets are stored within a top-level dict within the test data JSON
/// file, with string keys.
#define TEST_DATA_FOURS   "fours"


namespace PLDecimate_Test {

  struct PLDecimatetest : public NumPlTest::TestData {

    PLDecimatetest()
      : TestData( TEST_DATA_PATH"/decimate_test_data.json" )
    {;}

  };



  /* Define a singleton test data instance which is available across
   * test cases.
   *
   * The singleton reduces the boilerplate in each test case,
   * and lets us cache the test data.   Technically,
   * use of the singleton isn't mandatory, could just load the
   * data in each test case.
   *
   * \todo Perhaps this could be done with a GoogleTest
   *       fixture instead?
   */
  inline PLDecimatetest &theInstance() {
    static PLDecimatetest testDataClass;
    return testDataClass;
  }

  template <typename T>
  cv::Mat_<T> testData( const std::string &key=TEST_DATA_FOURS ) {
    return theInstance().operator()<T>( key );
  }

}
