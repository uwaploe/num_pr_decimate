#pragma once

#include "pl_routine.h"

namespace Numurus {

class PLDecimate : public PLRoutine
{
public:

  // \brief Structure to store configuration
  //
  struct PLDecimateConfig {

    // Default configuration values
    static const std::string TimeDecimateName;
    static const std::string RowDecimateName;
    static const std::string ColDecimateName;
    static const std::string ChannelDecimateName;
    static const std::string OperationName;

    enum DecimateOperation {
      OP_SUM = 0,
      OP_COUNT = 1
    };


    // Default constructor sets reasonable defaults
    PLDecimateConfig()
      : timeDecimate(1),
        rowDecimate(1),
        colDecimate( 1 ),
        channelDecimate( 1 ),
        operation( OP_SUM )
    {;}

    //! Extracts relevant configuration info from
    //! maps within PLRoutine::Config.
    //!
    //! \return frue if successful, false if a value is out of bounds
    virtual bool configure(const PLRoutine::Config &config);

    //! Returns the decimation arguments as a vector
    //!
    //! \return std::vector<int> of size 4 containing decimation args
    std::vector<int> decimateVector() const;

    //! Returns the product of all of the decmiation args
    //! (that is, the volume of the decimation "blocks")
    ///
    ///! \return
    int decimateProduct() const;

    //! Number of cross-correlation iterations to perform on the data.
    //! Must be between 1 and 4.
    int timeDecimate, rowDecimate, colDecimate, channelDecimate;
    int operation;

  protected:

    int getDecimateValue( const PLRoutine::Config &newConfig, const std::string &configName );

  };

  //! Default constructor
  PLDecimate();

  //! Destructor
  virtual ~PLDecimate();

  //! \brief Configure this PR from a PLRoutine::Config.
  //!
  //! \return true on success, false on an error (parameter out of bounds)
  virtual bool configure(const PLRoutine::Config &config);

  //! Process data.
  virtual bool process(cv::Mat *data_in, cv::Mat *data_out, float quality_in, float *quality_out);

protected:

  //! Local copy of configuration values
  PLDecimateConfig _decimateConfig;

};

}
