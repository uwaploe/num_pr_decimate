#!/usr/bin/env python3

import numpy as np
import cv2
import scipy.stats as stats

import itertools

import json

def mat_to_json( mat, shape=None ):

    if not shape:
        shape = mat.shape

    while(len(shape) < 4):
        shape = shape + (1,)

    m = { 'samples': shape[0],
          'rows': shape[1],
          'cols': shape[2],
          'channels': shape[3],
          'dtype': str(mat.dtype),
          'data': mat.flatten().tolist() }

    return m


def rpm2radpersec(rpm):
    return rpm/60*(2*np.pi)



if __name__ == '__main__':

    # Dict to accumulate test data.  Serialized to file at end of main()
    test_data = {}

    ## Detector sampling freq and length of buffer
    sband_hz=100
    buffer_len_secs=60

    ## Fixed random seed for debugging
    np.random.seed(0)

    buffer_len = sband_hz*buffer_len_secs

    # ***** Generate the all-zeroes data set
    test_data['fours'] = mat_to_json( np.ones( (4,4,4,4), dtype=np.int8 ) )

    # ***** Generate decimation results
    for dec in itertools.product( [1,2,4], repeat=4 ):

        sz = [ int(4/x) for x in dec ]

        mult = 1
        for i in dec:
            mult = mult * i

        key = "fours_decimate_" + ''.join([str(x) for x in dec])
        test_data[key] = mat_to_json( np.ones( sz, dtype=np.int8 ) * mult )


    ### Write to file
    with open( "decimate_test_data.json", 'w' ) as f:
      json.dump( test_data, f, indent=2 )
