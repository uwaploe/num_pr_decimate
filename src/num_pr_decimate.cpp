
#include <iostream>

#include <vector>
#include <algorithm>

#include <opencv2/imgproc/imgproc.hpp>

#include "num_pr_decimate/num_pr_decimate.h"


namespace Numurus
{

  const std::string PLDecimate::PLDecimateConfig::TimeDecimateName = "time_decimate";
  const std::string PLDecimate::PLDecimateConfig::RowDecimateName = "row_decimate";
  const std::string PLDecimate::PLDecimateConfig::ColDecimateName = "col_decimate";
  const std::string PLDecimate::PLDecimateConfig::ChannelDecimateName = "channel_decimate";
  const std::string PLDecimate::PLDecimateConfig::OperationName = "operation";

  using cv::Mat;
  using std::vector;

  //===

  class LoopFunctor {
  public:

    LoopFunctor( const cv::Mat &mat, const vector<int> decimate )
      :  _mat( mat ),
          _working(), _output(),
          _decimate(decimate),
          _sz(4,0)
      {

        std::vector<int> outputSize(4,0);
        for( int i = 0; i < 4; i++ ) {
          _sz[i] = _mat.size[i];
          outputSize[i] = ceil( float(_sz[i]) / _decimate[i] );
        }

        // Internally, use a float
        _mat.convertTo( _working, CV_32F );
        _output = cv::Mat( outputSize, CV_32F );
      }

      cv::Mat run() {
        _output = 0; // fill with 0

        cv::Vec<int,4> outPtr;
        vector<cv::Range> ranges(4, cv::Range(0,0) );

        run( 0, outPtr, ranges );

        cv::Mat out;
        _output.convertTo( out, _mat.type() );
        return out;
      }


    void run( int i, cv::Vec<int,4> outPtr, vector< cv::Range > ranges ) {

      const int count = ceil( _sz[i] / _decimate[i] );

      for( int x = 0; x < count; x++ ) {
        outPtr[i] = x;

        const cv::Range range( x*_decimate[i],  std::min( (x+1)*_decimate[i], _sz[i] ) );
        ranges[i] = range;

        if( i == 3 ) {
          // for( int j = 0; j < 4; ++j ) {
          //   std::cerr << j << " : range = " << ranges[j].start << " -- " << ranges[j].end << std::endl;
          // }

          const auto value = operation( cv::Mat( _working, ranges ) );
          _output.at<float>(outPtr) = value;
        } else {
          // Otherwise, recurse
          run( i+1, outPtr, ranges );
        }

      }

    }

    virtual float operation( const cv::Mat &roi ) = 0;

    const cv::Mat &output() const { return _output; }

    const cv::Mat _mat;
    cv::Mat _working, _output;
    const vector<int> _decimate;
    vector<int> _sz;

  };

  class SumFunctor : public LoopFunctor {
  public:

    SumFunctor( const cv::Mat &mat, const vector<int> decimate )
      :  LoopFunctor( mat, decimate )
      {;}

    virtual float operation( const cv::Mat &roi ) {
      double total = 0;

      cv::Vec<int,4> idx(4,0);
      for( idx[0] = 0; idx[0] < roi.size[0]; idx[0]++ ) {
        for( idx[1] = 0; idx[1] < roi.size[1]; idx[1]++ ) {
          for( idx[2] = 0; idx[2] < roi.size[2]; idx[2]++ ) {
            for( idx[3] = 0; idx[3] < roi.size[3]; idx[3]++ ) {
              total += roi.at<float>( idx );
              //std::cerr << "At " << idx << " ; total = " << total << "; roi=" << roi.at<double>( idx ) << std::endl;
            }
          }
        }
      }

      return total;
    }
  };

  //===



  PLDecimate::PLDecimate()
    : PLRoutine(),
      _decimateConfig()
  {
    // Set reasonable defaults
    config_.name_ = "PLDecimate";
    config_.description_ = "Performs decimation operations on input data.";
  }

  PLDecimate::~PLDecimate()
  {;}

  bool PLDecimate::configure(const PLRoutine::Config &newConfig)
  {
    if( !_decimateConfig.configure(newConfig) ) return false;

    return PLRoutine::configure(newConfig);
  }

  bool PLDecimate::process(Mat *data_in, Mat *data_out, float quality_in, float *quality_out)
  {
      // As a fallback, don't change quality
      *quality_out = quality_in;

      // Working data format is an int vector of length t, convert all other
      // data formats to this shape
      if( data_in == nullptr || data_out == nullptr ) {
        log( PL_ERROR, "Received null ptr for input / output data");
        return false;
      }

      if( !data_in->isContinuous() ) {
        log( PL_ERROR, "Received null ptr for input / output data");
        return false;
      }

      if( _decimateConfig.operation == PLDecimateConfig::OP_SUM ) {
        SumFunctor func( *data_in, _decimateConfig.decimateVector() );
        *data_out = func.run();
      } else {
        log( PL_ERROR, "Undefined operation.");
        return false;
      }

      double minVal = 0, maxVal = 0;
      cv::minMaxIdx( *data_out, &minVal, &maxVal, NULL, NULL );
      if( _decimateConfig.decimateProduct() != 0 ) {
        *quality_out = maxVal / _decimateConfig.decimateProduct();

        if( *quality_out > 1.0 ) *quality_out = 1.0;
        if( *quality_out < 0.0 ) *quality_out = 0.0;

      }

      return true;
  }

  //== PLDecimateConfig

  bool PLDecimate::PLDecimateConfig::configure(const PLRoutine::Config &newConfig)
  {
    {
      int val = getDecimateValue( newConfig, TimeDecimateName );
      if( val < 0 ) return false;
      timeDecimate = val;
    }

    {
      int val = getDecimateValue( newConfig, RowDecimateName );
      if( val < 0 ) return false;
      rowDecimate = val;
    }

    {
      int val = getDecimateValue( newConfig, ColDecimateName );
      if( val < 0 ) return false;
      colDecimate = val;
    }

    {
      int val = getDecimateValue( newConfig, ChannelDecimateName );
      if( val < 0 ) return false;
      channelDecimate = val;
    }

    if( newConfig.int_params.count( OperationName ) > 0 ) {
      const int op = newConfig.int_params.at( OperationName );
      if( op < 0 || op >= OP_COUNT ) return false;
      operation = op;
    }

    return true;
  }

  int PLDecimate::PLDecimateConfig::getDecimateValue( const PLRoutine::Config &newConfig, const std::string &configName ){

    if( newConfig.int_params.count( configName ) > 0 ) {
      const int val = newConfig.int_params.at( configName );
      if( val > 0  ) return val;
    }

    return -1;
  }


  std::vector<int> PLDecimate::PLDecimateConfig::decimateVector() const {
    std::vector<int> output(4);
    output[0] = timeDecimate;
    output[1] = rowDecimate;
    output[2] = colDecimate;
    output[3] = channelDecimate;

    return output;
  }

  int PLDecimate::PLDecimateConfig::decimateProduct() const {
    return timeDecimate * rowDecimate * colDecimate * channelDecimate;
  }


}
