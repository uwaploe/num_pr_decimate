# Minimum working example of Numurus Pipeline processing

Gitlabs CI Status: [![pipeline status](https://gitlab.com/apl-ocean-engineering/numurus_oot/num_pipeline_sband_freq_counter/badges/master/pipeline.svg)](https://gitlab.com/apl-ocean-engineering/numurus_oot/num_pipeline_sband_freq_counter/commits/master)

Builds a minimum working example of a Numurus Pipeline library processing
node.  Depends on [num_pl_core](https://bitbucket.org/numurus/num_pl_core.git).
